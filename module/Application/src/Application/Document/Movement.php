<?php
/**
 * @author: Wojciech Iskra <wojciech.iskra@schibsted.pl>
 */

namespace Application\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Movement extends AbstractDocument {

    const STATUS_FREE = 'free';
    const STATUS_OCCUPIED = 'occupied';
    const STATUS_GAMEON = 'gameOn';
    /**
     * Event id
     *
     * @var int
     * @ODM\Id
     */
    protected $movementId;

    /**
     * Creation datetime
     *
     * @var string
     * @ODM\Date
     */
    protected $createdAt;

    /**
     * Photo filename
     *
     * @var string
     * @ODM\String(nullable=false)
     */
    protected $photo;

    /**
     * Photo filename
     *
     * @var string
     * @ODM\String(nullable=false)
     */
    protected $room;

    /**
     * Event document constructor
     */
    public function __construct() {
        $this->setCreatedAt(new \Datetime());
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt() {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param int $movementId
     */
    public function setMovementId($movementId) {
        $this->movementId = $movementId;
    }

    /**
     * @return int
     */
    public function getMovementId() {
        return $this->movementId;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo) {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * @param string $room
     */
    public function setRoom($room) {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getRoom() {
        return $this->room;
    }

    public function getStatusName() {
        if ($this->getCreatedAt() < date('Y-m-d H:i:s', strtotime('2 minutes ago'))) {
            return self::STATUS_FREE;
        } else {
            return self::STATUS_GAMEON;
        }

    }

    public function getMovementInfo() {
        $movement = null;
        if ($this->getStatusName() == self::STATUS_GAMEON) {
            $movement = [
                'startDate' => $this->getCreatedAt(),
                'photo' => $this->getPhoto(),
            ];
        }

        return $movement;
    }

}
