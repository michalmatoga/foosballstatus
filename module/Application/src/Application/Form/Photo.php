<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form as ZendForm;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;

class Photo extends ZendForm
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        // File Input
        $file = new Element\File('photo_jpg');
        $file->setLabel('Movement photo');
        $this->add($file);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter();
        // File Input

        $fileInput = new FileInput('photo_jpg');
        $fileInput->setRequired(true);
        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            array(
                'target'    => PHOTO_PATH . '/photo.jpg',
                'randomize' => false,
                'use_upload_name' => true,
                'overwrite' => true,
            )
        );
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
    }
}
