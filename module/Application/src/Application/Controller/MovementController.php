<?php
/**
 * @author: Wojciech Iskra <wojciech.iskra@schibsted.pl>
 */

namespace Application\Controller;

require_once __DIR__.'/../../../../../vendor/google/apiclient/src/Google/Client.php';
require_once __DIR__.'/../../../../../vendor/google/apiclient/src/Google/Service/Calendar.php';

use Application\Document\AbstractDocument;
use Application\Document\Movement;
use Application\Form\Photo;
use Zend\Cache\StorageFactory;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class MovementController extends AbstractRestfulController {

    public function create($data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');

        $form = new Photo('upload-form');

        $request = $this->getRequest();
        // Make certain to merge the files info!
        $post = array_merge_recursive(
            $data,
            ['room' => $this->params('room')],
            $request->getFiles()->toArray()
        );
        $form->setData($post);

        if ($form->isValid()) {
            $data = $form->getData();
            $movement = new Movement();
            $movement->setPhoto($data['photo_jpg']['name']);
            $movement->setRoom($this->params('room'));
            $dm->persist($movement);
            $dm->flush();

            return $this->getResponse()->setStatusCode(200);
        }
        return $this->getResponse()->setStatusCode(403);
    }

    public function getList() {
        $result = $this->getLastMovement($this->params('room'));

        $resultArray = $result instanceof AbstractDocument ? $result->toArray() : null;

        return new JsonModel($resultArray);
    }

    public function statusAction() {
        $lastMovement = $this->getLastMovement($this->params('room'));

        $status = $this->getStatus($lastMovement);

        if ($status == Movement::STATUS_OCCUPIED) {
            $event = $this->getCurrentCalendarEvent();
            $calendar = new JsonModel(
                [
                    'title' => 'test',
                    'eventOwner' => $event->getOwner()->getName(),
                    'startDate' => date('Y-m-d H:i:s', strtotime($event->getStart()->getDateTime())),
                    'startDate' => date('Y-m-d H:i:s', strtotime($event->getEnd()->getDateTime()))
                ]
            );
        } else {
            $calendar = null;
        }

        return new JsonModel(
            [
                'status' => $status,
	            'lastStatusChange' => $lastMovement->getCreatedAt(),
                'movement' => $lastMovement->getMovementInfo(),
                'calendar' => $calendar,
            ]
        );
    }

    private function getCurrentCalendarEvent() {
        $now = time();
        foreach ($this->getCalendarResults() as $event) {
            if ($now > strtotime($event->getStart()->getDateTime()) && $now <= strtotime($event->getEnd()->getDateTime())) {
                return $event;
            }
        }
        return null;
    }

    private function getLastMovement($room) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $qb = $dm->createQueryBuilder('Application\Document\Movement');
        $result = $qb
            ->sort('createdAt', 'desc')
            ->getQuery()
            ->getSingleResult();

        return $result;
    }

    private function isOccupied() {
        return (bool) $this->getCurrentCalendarEvent();
    }
    private function getStatus($lastMovement) {
        if (!$lastMovement instanceof Movement || $movementStatus == Movement::STATUS_FREE){
            if ($this->isOccupied()) {
                return Movement::STATUS_OCCUPIED;
            }
        }
        return $movementStatus;
    }

    private function getCalendarResults() {
        // Via factory:
        $cache = StorageFactory::factory(array(
            'adapter' => 'filesystem',
            'cache_dir' => 'data/cache',
            'ttl' => 3600*24,
            'plugins' => array(
                'exception_handler' => array('throw_exceptions' => false),
            ),
        ));

        $key    = 'calendarResults';
        $results = $cache->getItem($key, $success);
        if (!$success) {
            $client = new \Google_Client();
            $client->setApplicationName('Foosball Status');
            $client->setClientId('400740340098-sprd9k1n7se8do5rk6pd78uj3f9ocbhu.apps.googleusercontent.com');
            $client->setClientSecret('nUiFAblP_Zh-7s5X_wSg1HnR');
            $client->setRedirectUri('http://localhost/foosballstatus/api/corner/status');
            $client->setDeveloperKey('AIzaSyB67ysoX_mb6C4SPbCiEttEfx3ja2-cnRM');

            $service = new \Google_Service_Calendar($client);

            if (isset($_GET['logout'])) { // logout: destroy token
                unset($_SESSION['token']);
                die('Logged out.');
            }

            if (isset($_GET['code'])) { // we received the positive auth callback, get the token and store it in session
                $client->authenticate($_GET['code']);
                $_SESSION['token'] = $client->getAccessToken();
            }

            if (isset($_SESSION['token'])) { // extract token from session and configure client
                $token = $_SESSION['token'];
                $client->setAccessToken($token);
            }

            if (!$client->getAccessToken()) { // auth call to google
                $authUrl = $client->createAuthUrl();
                header("Location: " . $authUrl);
                die;
            }
            $results = $service->events->listEvents('schibsted.pl_35373139363931392d383530@resource.calendar.google.com'
                ,array(
                    'orderBy' => 'startTime',
                    'singleEvents' => 'true',
                    'timeMin' => date(\DateTime::ATOM, strtotime('today')),
                    'timeMax' => date(\DateTime::ATOM, strtotime('tomorrow')),
                    'timeZone' => 'Europe/Warsaw',
                    'fields' => 'items(summary,creator(displayName,email),description,end,start,status)')
            );
            $cache->setItem($key, $results);
        }

        return $results;
    }
}
